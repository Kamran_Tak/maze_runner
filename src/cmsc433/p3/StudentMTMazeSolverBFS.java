package cmsc433.p3;

import java.util.concurrent.*;

import java.util.LinkedList;
import java.util.List;

/**
 * This file needs to hold your solver to be tested. 
 * You can alter the class to extend any class that extends MazeSolver.
 * It must have a constructor that takes in a Maze.
 * It must have a solve() method that returns the datatype List<Direction>
 *   which will either be a reference to a list of steps to take or will
 *   be null if the maze cannot be solved.
 */
public class StudentMTMazeSolverBFS extends SkippingMazeSolver
{
	private final ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
//	private final ExecutorService executor = Executors.newFixedThreadPool(2);

	//	Executors.newCachedThreadPool();
	//	Executors.newSingleThreadExecutor();
	static SolutionNode curr = null;
	static volatile boolean resultFound;

	static LinkedBlockingQueue<LinkedList<SolutionNode>> sharedResult = new LinkedBlockingQueue<LinkedList<SolutionNode>>();

	public StudentMTMazeSolverBFS(Maze maze)
	{
		super(maze);

	}

	public List<Direction> solve() {

		LinkedList<SolutionNode> starter = new LinkedList<SolutionNode>();
		try {
			starter.push(new SolutionNode(null, firstChoice(maze.getStart())));
		} catch (SolutionFound e1) {
			return solutionFoundHandler();
		}

		//		synchronized(sharedResult) {
		try {
			sharedResult.put(starter);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//		}

		LinkedList<Future<List<Direction>>> futureList = new LinkedList<Future<List<Direction>>>();

		for(int i = 0; i < Runtime.getRuntime().availableProcessors(); i++) {

			futureList.add(executor.submit(new Callable<List<Direction>>() {

				public List<Direction> call() throws SolutionFound  { //throws SolutionFound
					LinkedList<SolutionNode> frontier = new LinkedList<SolutionNode>();
					try {
						//						synchronized(sharedResult) {
						frontier = sharedResult.take();
						//						}
					} catch (InterruptedException e1) {
						if (resultFound)
							return null;
					}

					while (!frontier.isEmpty()) {
						if (resultFound)
							return null;
						for (SolutionNode node : frontier) {
							if (!node.choice.isDeadend()) {
								expand(node);
							}
							else if (maze.display != null) {
								maze.setColor(node.choice.at, 0);
							}
						}
						try {
							//							synchronized(sharedResult){
							frontier = sharedResult.take();
							//							}
						} catch (InterruptedException e) {
							if (resultFound)
								return null;
							//e.printStackTrace();
						}
					}
					// No solution found.
					return null;
				}
			}));
		}

		List<Direction> solution = null;



		for (Future<List<Direction>> f : futureList) {
			//			if (sharedResult.isEmpty() && !resultFound)
			//				f.cancel(true);
			try {
				solution = f.get();
			} catch (InterruptedException e) {
			} catch (ExecutionException e) {
				//				if (resultFound)
				if (e.getCause().getClass().equals(SkippingMazeSolver.SolutionFound.class))
					executor.shutdownNow();
				return solutionFoundHandler();
			}
		}
		return solution;

	}

	public class SolutionNode
	{
		public SolutionNode parent;
		public Choice choice;

		public SolutionNode(SolutionNode parent, Choice choice)
		{
			this.parent = parent;
			this.choice = choice;
		}
	}

	Direction exploring = null;


	/**
	 * Expands a node in the search tree, returning the list of child nodes.
	 * 
	 * @throws SolutionFound
	 */
	public void expand(SolutionNode node) throws SolutionFound
	{
		LinkedList<SolutionNode> result = new LinkedList<SolutionNode>();
		if (maze.display != null) maze.setColor(node.choice.at, 0);
		Choice newChoice;
		for (Direction dir : node.choice.choices)
		{
			try {
				newChoice = follow(node.choice.at, dir);
			} catch (SolutionFound e) {
				exploring = dir;
				curr = node;
				resultFound = true;
				throw e;
			}
			if (maze.display != null) maze.setColor(newChoice.at, 2);
			result.add(new SolutionNode(node, newChoice));
		}
		try {
			//			synchronized(sharedResult){
			sharedResult.put(result);
			//			}
		} catch (InterruptedException e) {
			if (resultFound)
				return;
			//			e.printStackTrace();
		}
	}

	public List<Direction> solutionFoundHandler() {
		//e.getCause().printStackTrace();
		resultFound = true;
		executor.shutdownNow();

		if (curr == null)
		{
			// this only happens if there was a direct path from the start
			// to the end
			return pathToFullPath(maze.getMoves(maze.getStart()));
		}
		else
		{
			System.out.println("curr = " + curr.choice.at);
			LinkedList<Direction> soln = new LinkedList<Direction>();
			// First save the direction we were going in when the exit was
			// discovered.
			soln.addFirst(exploring);
			while (curr != null) {
				try {
					Choice walkBack = followMark(curr.choice.at, curr.choice.from, 1);
					if (maze.display != null) {
						maze.display.updateDisplay();
					}
					soln.addFirst(walkBack.from);
					curr = curr.parent;
				}
				catch (SolutionFound e2) {
					// If there is a choice point at the maze entrance, then
					// record
					// which direction we should choose.
					if (maze.getMoves(maze.getStart()).size() > 1) soln.addFirst(e2.from);
					if (maze.display != null)
					{
						markPath(soln, 1);
						maze.display.updateDisplay();
					}
					executor.shutdownNow();
					return pathToFullPath(soln);
				}
			}
			markPath(soln, 1);
			executor.shutdownNow();
			return pathToFullPath(soln);
		}
	}

	//
	//	/**
	//	 * Expands a node in the search tree, returning the list of child nodes.
	//	 * @return 
	//	 * 
	//	 * @throws SolutionFound
	//	 */
	//	public Future<LinkedList<SolutionNode>> expand(final SolutionNode node) throws SolutionFound
	//	{
	//		//		final LinkedList<SolutionNode> result = new LinkedList<SolutionNode>();
	//		if (maze.display != null) maze.setColor(node.choice.at, 0);
	//
	//		//		LinkedList<Future<SolutionNode>> futureList = new LinkedList<Future<SolutionNode>>();
	//		final LinkedList<SolutionNode> solutionList = new LinkedList<SolutionNode>();
	//
	//		Future<LinkedList<SolutionNode>> future = executor.submit(new Callable<LinkedList<SolutionNode>>() {
	//			public LinkedList<SolutionNode> call() throws SolutionFound {
	//				Choice newChoice;
	//
	//				for (final Direction dir : node.choice.choices) {
	//					try {
	//						newChoice = follow(node.choice.at, dir);
	//					} catch (SolutionFound sf) {
	//						exploring = dir;
	//						curr = node;
	//						throw sf;
	//					}
	//
	//					if (maze.display != null) maze.setColor(newChoice.at, 2);
	//					solutionList.add(new SolutionNode(node, newChoice));
	//				}
	//				return solutionList;
	//			}
	//		});
	//
	//		return future;
	//	}
	//
	//
	//
	//	/**
	//	 * Performs a breadth-first search of the maze. The algorithm builds a tree
	//	 * rooted at the start position. Parent pointers are used to point the way
	//	 * back to the entrance. The algorithm stores the list of leaves in the
	//	 * variables "frontier". During each iteration, these leaves are each
	//	 * expanded and the children the result become the new frontier. If a node
	//	 * represents a dead-end, it is discarded. Execution stops when the exit is
	//	 * discovered, as indicated by the SolutionFound exception.
	//	 */
	//	public List<Direction> solve()
	//	{
	//		LinkedList<SolutionNode> frontier = new LinkedList<SolutionNode>();
	//		LinkedList<SolutionNode> futureList = null;
	//
	//		//		LinkedList<SolutionNode> frontier = new LinkedList<SolutionNode>();
	//
	//		try {
	//			frontier.push(new SolutionNode(null, firstChoice(maze.getStart())));
	//		} catch (SolutionFound e) {
	//			return solutionFoundHandler();
	//		}
	//		while (!frontier.isEmpty()) {
	//			LinkedList<SolutionNode> new_frontier = new LinkedList<SolutionNode>();
	//			for (SolutionNode node : frontier) {
	//				if (!node.choice.isDeadend()) {
	//					try {
	//						futureList = expand(node).get();
	//					} catch (SolutionFound e) {
	//						return solutionFoundHandler();
	//					} catch (InterruptedException e) {
	//
	//					} catch (ExecutionException e) {
	//						return solutionFoundHandler();
	//					}
	//
	//					SolutionNode new_choice;
	//
	//					for (SolutionNode future : futureList) {
	//						new_choice = future;
	//						new_frontier.add(new_choice);
	//					}
	//				}
	//				else if (maze.display != null)
	//				{
	//					maze.setColor(node.choice.at, 0);
	//				}
	//			}
	//			frontier = new_frontier;
	//			//				if (maze.display != null)
	//			//				{
	//			//					maze.display.updateDisplay();
	//			//					try
	//			//					{
	//			//						Thread.sleep(5);
	//			//					}
	//			//					catch (InterruptedException e)
	//			//					{
	//			//					}
	//			//				}
	//		}
	//		// No solution found.
	//		return null;
	//	}
}

