package cmsc433.p3;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import cmsc433.p3.STMazeSolverBFS.SolutionNode;

/**
 * An efficient single-threaded depth-first solver.
 */
public class StudentMTMazeSolver extends SkippingMazeSolver
{

	private final ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
	int numChoices = 0;

	public StudentMTMazeSolver(Maze maze)
	{
		super(maze);
	}

	public List<Direction> solve() {

		//		final LinkedList<Choice> choiceStack1 = new LinkedList<Choice>();
		LinkedList<Choice>[] choiceStackList;

		Choice ch;


		try {
			ch = firstChoice(maze.getStart());		
		} catch (SolutionFound e) {
			return pathToFullPath(new LinkedList<Direction>());
		}
		//		choiceStack1.push(ch);
		numChoices = ch.choices.size();
		//		LinkedList<Choice> choiceStack2;
		final Choice[] firstChoice = new Choice[numChoices];

		int i = 0;
		LinkedList<Direction> firstChoiceDirecs;
		for (Direction d : ch.choices) {
			firstChoiceDirecs = new LinkedList<Direction>();
			firstChoiceDirecs.push(d);
			firstChoice[i++] = new Choice(ch.at, ch.from, firstChoiceDirecs);
		}
		LinkedList<Choice>[][] secondChoice = (LinkedList<Choice>[][]) new LinkedList<?>[numChoices][];

		int numChoices2;
		Choice newChoice;
		Choice choiceIJ;
		LinkedList<Direction> secondChoiceDirecs;
		int j;

		for (i = 0; i < numChoices; i++) {

			try {
				newChoice = follow(firstChoice[i].at, firstChoice[i].choices.peek());
				//				firstChoice[i].choices.clear();
			} catch (SolutionFound e) {
				//				System.out.println("Second try/catch");
				LinkedList<Direction> solutionPath = new LinkedList<Direction>();
				solutionPath.push(firstChoice[i].choices.peek());
				return pathToFullPath(solutionPath);
			}

			numChoices2 = newChoice.choices.size();
			secondChoice[i] = (LinkedList<Choice>[]) new LinkedList<?>[numChoices2];

			j = 0;
			for (Direction d : newChoice.choices) {
				secondChoice[i][j] = new LinkedList<Choice>();
				//				secondChoice[i][j].push(firstChoice[i]);
				secondChoiceDirecs = new LinkedList<Direction>();
				secondChoiceDirecs.push(d);
				choiceIJ = new Choice(newChoice.at, newChoice.from, secondChoiceDirecs);
				secondChoice[i][j].push(choiceIJ);
				j++;
			}
		}

		LinkedList<Future<List<Direction>>> futureList = new LinkedList<Future<List<Direction>>>();

		for (final LinkedList<Choice> c1 : secondChoice[0]) {
			//				System.out.println(c1.peek().choices);
			futureList.add(executor.submit(new Callable<List<Direction>>() {
				public List<Direction> call() {
					LinkedList<Choice> choiceStack = c1;
					//					System.out.println(c1.peek().choices);

					Choice ch;

					//						choiceStack.push(firstChoice(maze.getStart()));
					//						while (!choiceStack.isEmpty()) {
					try {
						while (!choiceStack.isEmpty()) {
							//						while (choiceStack.size() > 1) {
							ch = choiceStack.peek();
							if (ch.isDeadend())	{
								// backtrack.
								choiceStack.pop();
								if (!choiceStack.isEmpty())
									choiceStack.peek().choices.pop();
								continue;
							}
							choiceStack.push(follow(ch.at, ch.choices.peek()));
						}
						return null;
					} catch (SolutionFound e) {
//						System.out.println("Third try/catch");
						executor.shutdownNow();
						Iterator<Choice> iter = choiceStack.iterator();
						LinkedList<Direction> solutionPath = new LinkedList<Direction>();
						while (iter.hasNext()) {
							ch = iter.next();
							solutionPath.push(ch.choices.peek());
							//System.out.println(ch.from + " -> " + ch.at + ": " + ch.choices.peek());
						}
						solutionPath.push(firstChoice[0].choices.peek());
						//							solutionPath.push();

						if (maze.display != null) maze.display.updateDisplay();
						//						System.out.println(solutionPath);
						return pathToFullPath(solutionPath);
					}
				}
			}));
		}

		if (numChoices >= 2) {

			for (final LinkedList<Choice> c2 : secondChoice[1]) {
				//				System.out.println(c1.peek().choices);
				futureList.add(executor.submit(new Callable<List<Direction>>() {
					public List<Direction> call() {
						LinkedList<Choice> choiceStack = c2;
						//					System.out.println(c1.peek().choices);

						Choice ch;

						//						choiceStack.push(firstChoice(maze.getStart()));
						//						while (!choiceStack.isEmpty()) {
						try {
							while (!choiceStack.isEmpty()) {
								//							while (choiceStack.size() > 1) {
								ch = choiceStack.peek();
								if (ch.isDeadend())	{
									// backtrack.
									choiceStack.pop();
									if (!choiceStack.isEmpty())
										choiceStack.peek().choices.pop();
									continue;
								}
								choiceStack.push(follow(ch.at, ch.choices.peek()));
							}
							return null;
						} catch (SolutionFound e) {
//							System.out.println("Third try/catch");
							executor.shutdownNow();
							Iterator<Choice> iter = choiceStack.iterator();
							LinkedList<Direction> solutionPath = new LinkedList<Direction>();
							while (iter.hasNext()) {
								ch = iter.next();
								solutionPath.push(ch.choices.peek());
								//System.out.println(ch.from + " -> " + ch.at + ": " + ch.choices.peek());
							}
							solutionPath.push(firstChoice[1].choices.peek());
							//							solutionPath.push();

							if (maze.display != null) maze.display.updateDisplay();
							//							System.out.println(solutionPath);
							return pathToFullPath(solutionPath);
						}
					}
				}));





			}
		}

		if (numChoices >= 3) {

			for (final LinkedList<Choice> c2 : secondChoice[2]) {
				//				System.out.println(c1.peek().choices);
				futureList.add(executor.submit(new Callable<List<Direction>>() {
					public List<Direction> call() {
						LinkedList<Choice> choiceStack = c2;
						//					System.out.println(c1.peek().choices);

						Choice ch;

						//						choiceStack.push(firstChoice(maze.getStart()));
						//						while (!choiceStack.isEmpty()) {
						try {
							while (!choiceStack.isEmpty()) {
								//							while (choiceStack.size() > 1) {
								ch = choiceStack.peek();
								if (ch.isDeadend())	{
									// backtrack.
									choiceStack.pop();
									if (!choiceStack.isEmpty())
										choiceStack.peek().choices.pop();
									continue;
								}
								choiceStack.push(follow(ch.at, ch.choices.peek()));
							}
							return null;
						} catch (SolutionFound e) {
//							System.out.println("Third try/catch");
							executor.shutdownNow();
							Iterator<Choice> iter = choiceStack.iterator();
							LinkedList<Direction> solutionPath = new LinkedList<Direction>();
							while (iter.hasNext()) {
								ch = iter.next();
								solutionPath.push(ch.choices.peek());
								//System.out.println(ch.from + " -> " + ch.at + ": " + ch.choices.peek());
							}
							solutionPath.push(firstChoice[2].choices.peek());
							//							solutionPath.push();

							if (maze.display != null) maze.display.updateDisplay();
							//							System.out.println(solutionPath);
							return pathToFullPath(solutionPath);
						}
					}
				}));




			}
		}

		List<Direction> solution = null;
		List<Direction> tempSolution;

		for (Future<List<Direction>> f : futureList) {
			//			System.out.println(f);
			try {
				tempSolution = f.get();
				if (tempSolution != null) {
					solution = tempSolution;
					executor.shutdownNow();
					//					System.out.println(solution);
				}
			} catch (InterruptedException e) {
			} catch (ExecutionException e) {
				if (e.getCause().getClass().equals(SkippingMazeSolver.SolutionFound.class))
					executor.shutdownNow();
			}
		}
		executor.shutdownNow();
//		System.out.println(solution);
		return solution;
	}

}
